Simple module to allow hash in menu.

This module allows to add hash in the menu links. Additionally one can also add hash with a div ID required for scroll to actions.

You can use this module by using and use /(yourdivid) in the menu path.
